
FROM openjdk:17-jdk-alpine
MAINTAINER aryana0631
COPY target/paymentservice-0.0.1-SNAPSHOT.jar paymentservice-server-1.0.0.jar
EXPOSE 8081
ENTRYPOINT ["java","-jar","/paymentservice-server-1.0.0.jar"]