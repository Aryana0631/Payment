#!/bin/bash
docker rm --force payment
mvn clean package -DskipTests
docker image build -t payment .
docker container run --name payment -p 8081:8081 -d payment