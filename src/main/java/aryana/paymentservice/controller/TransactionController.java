package aryana.paymentservice.controller;

import aryana.paymentservice.entity.Transaction;
import aryana.paymentservice.entity.User;
import aryana.paymentservice.entity.Wallet;
import aryana.paymentservice.model.AddMoney;
import aryana.paymentservice.model.ResponseMessage;
import aryana.paymentservice.repository.TransactionRepository;
import aryana.paymentservice.repository.UserRepository;
import aryana.paymentservice.repository.WalletRepository;
import aryana.paymentservice.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TransactionController {

    @Autowired
    private TransactionService transactionService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private WalletRepository walletRepository;

    @PostMapping("/transaction/pay")
    ResponseEntity<?> payTransaction(@RequestBody Transaction newTransaction) {
        User user = userRepository.findByUserName(newTransaction.getUser().getUserName());
        Wallet wallet = user.getWallet();
        newTransaction.setUser(user);
        if (!transactionService.hasEnoughBalance(wallet, newTransaction)) {
            return new ResponseEntity<>(new ResponseMessage(false, "Not Enough Balance"), HttpStatus.OK);
        }
        transactionService.payTransaction(wallet, newTransaction);
        userRepository.save(user);
        transactionRepository.save(newTransaction);
        return new ResponseEntity<>(new ResponseMessage(true, "Payed"), HttpStatus.OK);
    }

    @PostMapping("/transaction/add_money")
    ResponseEntity<?> payTransaction(@RequestBody AddMoney addMoney) {
        User user = userRepository.findByUserName(addMoney.getUser().getUserName());
        user.getWallet().addMoney(addMoney.getValue());
        walletRepository.save(user.getWallet());
        return new ResponseEntity<>(new ResponseMessage(true, "Money Has Been Added To Your Wallet"), HttpStatus.OK);
    }



}
