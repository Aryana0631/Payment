package aryana.paymentservice.model;

public record ResponseMessage(boolean result, String message) {

}
