package aryana.paymentservice.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class OnlineShopUser {


    private String id;
    private String firstName;
    private String lastName;
    private String userName;
    private String email;
    private String phoneNumber;


}
