package aryana.paymentservice.model;


import aryana.paymentservice.entity.User;

public class AddMoney {

    private User user;
    private double value;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }
}
