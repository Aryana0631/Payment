package aryana.paymentservice.rabbitmq;

import aryana.paymentservice.model.OnlineShopUser;
import aryana.paymentservice.service.UserService;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.CountDownLatch;

@Component
public class Consumer {

    private CountDownLatch latch = new CountDownLatch(1);

    @Autowired
    private Gson gson;

    @Autowired
    private UserService userService;

    public void receiveMessage(String message) {
        OnlineShopUser onlineShopUser = gson.fromJson(message, OnlineShopUser.class);
        if(onlineShopUser != null){
            userService.createOrUpdateUserInfo(onlineShopUser);
        }
        System.out.println("Received <" + message + ">");
        latch.countDown();
    }

    public CountDownLatch getLatch() {
        return latch;
    }

}
