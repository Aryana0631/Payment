package aryana.paymentservice.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table
public class Transaction {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private User user;
    private double valueToBePayed;

    private boolean isPayed = false;

    private String moreInfo;

    @SequenceGenerator(
            name = "reference_code_sequence",
            sequenceName = "reference_code_sequence",
            initialValue = 1000000000
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "reference_code_sequence"
    )
    private Long referenceCode;

    public Transaction(Long id, User user, double valueToBePayed, boolean isPayed, String moreInfo) {
        this.id = id;
        this.user = user;
        this.valueToBePayed = valueToBePayed;
        this.isPayed = isPayed;
        this.moreInfo = moreInfo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public double getValueToBePayed() {
        return valueToBePayed;
    }

    public void setValueToBePayed(double valueToBePayed) {
        this.valueToBePayed = valueToBePayed;
    }

    public boolean isPayed() {
        return isPayed;
    }

    public void setPayed(boolean payed) {
        isPayed = payed;
    }

    public String getMoreInfo() {
        return moreInfo;
    }

    public void setMoreInfo(String moreInfo) {
        this.moreInfo = moreInfo;
    }

    public Long getReferenceCode() {
        return referenceCode;
    }

    public void setReferenceCode(Long referenceCode) {
        this.referenceCode = referenceCode;
    }


}
