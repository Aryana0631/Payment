package aryana.paymentservice.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;


@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table
public class Wallet {

    @Id
    @SequenceGenerator(
            name = "wallet_id_sequence",
            sequenceName = "wallet_id_sequence",
            initialValue = 1000000000
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "wallet_id_sequence"
    )
    private Integer id;

    private double balance;

    public Wallet(double balance) {
        this.balance = balance;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public void addMoney(double value) {
        this.balance += value;
    }
}
