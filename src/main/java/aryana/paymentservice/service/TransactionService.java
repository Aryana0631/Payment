package aryana.paymentservice.service;

import aryana.paymentservice.entity.Transaction;
import aryana.paymentservice.entity.Wallet;
import org.springframework.stereotype.Service;

@Service
public class TransactionService {


    public boolean hasEnoughBalance(Wallet wallet, Transaction transaction) {
        return wallet.getBalance() > transaction.getValueToBePayed();
    }


    public void payTransaction(Wallet wallet, Transaction transaction) {
        wallet.setBalance(wallet.getBalance() - transaction.getValueToBePayed());
        transaction.setPayed(true);
    }

}
