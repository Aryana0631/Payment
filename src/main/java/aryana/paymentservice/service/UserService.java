package aryana.paymentservice.service;

import aryana.paymentservice.entity.Transaction;
import aryana.paymentservice.entity.User;
import aryana.paymentservice.entity.Wallet;
import aryana.paymentservice.model.OnlineShopUser;
import aryana.paymentservice.repository.UserRepository;
import aryana.paymentservice.repository.WalletRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private WalletRepository walletRepository;

    public User createOrUpdateUserInfo(OnlineShopUser onlineShopUser) {
        User oldUser = userRepository.findByUserName(onlineShopUser.getUserName());
        if (oldUser == null) {
            oldUser = new User();
            oldUser.setId(onlineShopUser.getId());
            Wallet wallet = new Wallet(0);
            walletRepository.save(wallet);
            oldUser.setWallet(wallet);
        }
        oldUser.setFullName(onlineShopUser.getFirstName() + " " + onlineShopUser.getLastName());
        oldUser.setEmail(onlineShopUser.getEmail());
        oldUser.setPhoneNumber(onlineShopUser.getPhoneNumber());
        oldUser.setUserName(onlineShopUser.getUserName());
        return userRepository.save(oldUser);
    }

}
