package aryana.paymentservice.service;


import aryana.paymentservice.entity.Transaction;
import aryana.paymentservice.entity.User;
import aryana.paymentservice.entity.Wallet;
import org.springframework.stereotype.Service;

@Service
public class DefaultDataService {

    public User getDefaultUser() {
        return new User("Test", "Aryana0631","Aryana Azadi", "AryanaAzadiLive@gmail.com", "+449301661152");
    }

    public Wallet getDefaultWallet() {
        return new Wallet(1, 10000);
    }

    public Wallet getDefaultWalletWithZeroBalance() {
        return new Wallet(2, 0);
    }

    public Transaction getDefaultTransaction() {
        return new Transaction(1L, getDefaultUser(), 3000L, false, "Not Payed");
    }
}
