package aryana.paymentservice.transaction;

import aryana.paymentservice.Config;
import aryana.paymentservice.entity.Transaction;
import aryana.paymentservice.entity.Wallet;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TransactionServiceTest extends Config {


    @Test
    public void hasEnoughBalanceWithEnoughBalance() {
        Wallet wallet = defaultDataService.getDefaultWallet();
        Transaction transaction = defaultDataService.getDefaultTransaction();
        boolean payed = transactionService.hasEnoughBalance(wallet, transaction);
        assertThat(payed).isTrue();
    }

    @Test
    public void hasEnoughBalanceWithNotEnoughBalance() {
        Wallet wallet = defaultDataService.getDefaultWalletWithZeroBalance();
        Transaction transaction = defaultDataService.getDefaultTransaction();
        boolean payed = transactionService.hasEnoughBalance(wallet, transaction);
        assertThat(payed).isFalse();
    }

    @Test
    public void paymentAndSave() {
        Wallet wallet = defaultDataService.getDefaultWallet();
        Transaction transaction = defaultDataService.getDefaultTransaction();
        double remainingBalance = wallet.getBalance() - transaction.getValueToBePayed();
        transactionService.payTransaction(wallet, transaction);
        assertThat(wallet.getBalance()).isEqualTo(remainingBalance);
        assertThat(transaction.isPayed()).isEqualTo(true);
    }

    @Test
    public void addMoneyToWallet() {
        Wallet wallet = defaultDataService.getDefaultWallet();
        double value = 1000;
        double newValue = 1000 + wallet.getBalance();
        wallet.addMoney(value);
        assertThat(wallet.getBalance()).isEqualTo(newValue);
    }
}
