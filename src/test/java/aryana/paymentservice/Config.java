package aryana.paymentservice;

import aryana.paymentservice.repository.UserRepository;
import aryana.paymentservice.service.DefaultDataService;
import aryana.paymentservice.service.TransactionService;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.testcontainers.junit.jupiter.Testcontainers;

@Testcontainers
@SpringBootTest
public class Config {


    @SpyBean
    protected DefaultDataService defaultDataService;

    @SpyBean
    protected UserRepository userRepository;

    @SpyBean
    protected TransactionService transactionService;


}
